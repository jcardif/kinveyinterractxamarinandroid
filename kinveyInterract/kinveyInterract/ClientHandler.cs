﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Kinvey;
using SQLite.Net.Platform.XamarinAndroid;

namespace kinveyInterract
{
    public class ClientHandler
    {

        private static Client kinveyClient;
        private static string your_app_key= "kid_rkwYihYOb";
        private static string your_app_secret= "a0aabcfaa32c4087b9a7bf7dae66976f";

        public static Client GetClient()
        {
            if (kinveyClient == null)
            {


                #region Without Offline database
                Client.Builder builder = new Client.Builder(your_app_key, your_app_secret)
                    .setLogger(delegate (string msg) { Console.WriteLine(msg); })
                    .setFilePath(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal))
                    .setOfflinePlatform(new SQLitePlatformAndroid());
                #endregion

                kinveyClient = builder.Build();
            }

            return kinveyClient;
        }

    }
}