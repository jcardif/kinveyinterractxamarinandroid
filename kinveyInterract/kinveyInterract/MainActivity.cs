﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Kinvey;

namespace kinveyInterract
{
    [Activity(Label = "kinveyInterract", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText _email, _password;

        private Button _btn;

        //private static Client kinveyClient;
        private Context _context;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            _email = FindViewById<EditText>(Resource.Id.email);
            _password = FindViewById<EditText>(Resource.Id.password);
            _btn = FindViewById<Button>(Resource.Id.btn1);

            _btn.Click += _btn_Click;
        }

        private async void _btn_Click(object sender, System.EventArgs e)
        {
            _context = this;
            //Something
            if (_email.Text != null && _password.Text != null)
            {

                ClientHandler.GetClient().User().Login(_email.Text, _password.Text, new KinveyDelegate<User>
                {
                    onSuccess = (user) =>
                    {
                        StartActivity(typeof(DashBoardActivity));
                    },
                    onError = (error) =>
                    {
                        RunOnUiThread(() =>
                        {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                            builder1.SetMessage(error.Message);
                            builder1.SetCancelable(true);

                            AlertDialog alert1 = builder1.Create();
                            alert1.Show();

                        });

                    }

                });

            }
            else
            {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.SetMessage("Inavlid email or password");
                builder1.SetCancelable(true);

                AlertDialog alert1 = builder1.Create();
                alert1.Show();
            }
        }
    }
}

